q = [1,2,3,5,4,6,7,8]
# q = [2,1,5,3,4]
# # q_Expected = [i for i in range(min(q), len(q) + 1)]
# # # now see which is the digit that is out of sequence
# # indexOutOfSeq = 0
# # nActualVal = 0
# # for index, val in enumerate(q_Expected):
# #     nActualVal = q[index]
# #     if val != nActualVal:
# #         indexOutOfSeq = index
# #         break
    
# # now we know that 5 is out of sequence which is index 3
# # now see what is the difference between this misplaced digit and the digit after
# # in the actual queue that was supplied to us
# #nDifference = nActualVal - q[indexOutOfSeq + 1]
# nDifferences = []
# previousDigit = -1
# for nDigit in q:
#     if nDigit < previousDigit:
#         nDifferences.append(previousDigit - nDigit)
#     previousDigit = nDigit

# print(nDifferences)


# q_Expected = [i for i in range(min(q), len(q) + 1)]
# nDifferences = 0
# lTooChaotic = False
# for index, expectedVal in enumerate(q_Expected):
#     nActualVal = q[index]
#     if expectedVal < nActualVal:
#         nDifferences += (nActualVal - expectedVal)
#         if (nActualVal - expectedVal) > 2:
#             lTooChaotic = True
#             #print("Too chaotic")
#             break

# if lTooChaotic:
#     print("Too chaotic")
# else:
#     print(nDifferences)+


# I was doing it what the sequence should be and compare it with the sequence we get.
# the solution that works is what will it take to make it come back to the sequence
# we are moving from end to start
result=0
for l in range(len(q)-1,0,-1):
    if q[l]!=l+1:
        if q[l-1]==l+1:
            result+=1
            q[l-1],q[l]=q[l], q[l-1]
            
        elif q[l-2]==l+1:
            result+=2
            q[l-2],q[l-1],q[l]=q[l-1],q[l],q[l-2]
        else:
            print("Too chaotic")
            break
print(result)