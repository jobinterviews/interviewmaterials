related = ['1000001000', '0100010001', '0010100000', '0001000000', '0010100000', '0100010000', '1000001000', '0000000100', '0000000010', '0100000001']
nRows = len(related)
nCols = len(related[0]) # Assuming that all rows have the same amount of columns
print(related)
nFamilyCount = 0
lSharedBook = False
# Go through each row and see if there is a corresponding 1 
for rowNum,eachRow in enumerate(related):
    # go through each row to see if we have a 1 
    lSharedBook = False
    for colNum, nDigit in enumerate(eachRow):
        # see if this digit is a 0 or a 1
        if nDigit == "1":
            lSharedBook = True
            # This is a share. colNum is the person shared with and rowNum is who shared
            # Now see if the reverse is true or not
            if related[colNum][rowNum] == "1" and colNum > rowNum:
                # related[colNum][rowNum] = "0"
                nFamilyCount+=1
                
    if lSharedBook == False:
        nFamilyCount+=1

if nFamilyCount == 0:
    nFamilyCount = nRows
    
return nFamilyCount