#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'lonelyinteger' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY a as parameter.
#

def lonelyinteger(a):
    # Write your code here
    # First sort the array
    a.sort()
    # now all elemenst are in a sequence so the next number is the same as this number
    # unless we hit a digit where next and prior number is not the same. 
    # this is our unique number
    
    # better is make a set out of the array or list
    nUniqueNumbers = set(a)
    # all unique numbers are here. Now see which one is once
    for num in nUniqueNumbers:
        if a.count(num) == 1:
            return num

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    a = list(map(int, input().rstrip().split()))

    result = lonelyinteger(a)

    fptr.write(str(result) + '\n')

    fptr.close()
