#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'timeConversion' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
#

def timeConversion(s):
    # Write your code here
    # for a 24 hour midnight it is 00 for hour
    # format coming in is HH:MM:SSPM
    cAmPm = s[8:].upper()
    cTimeFormat = s[0:8]
    cTimeList = cTimeFormat.split(':')
    cHour = cTimeList[0]
    cRemainingTime = s[2:8]
    
    cTimeToReturn = ""
    
    if cAmPm == "AM":
        # Check if it is midnight or not
        if int(cHour) == 12:
            # this is a midnight time, convert it to 00
            cTimeToReturn = "00" + cRemainingTime
        else:
            cTimeToReturn = s[0:8]
    else:
        # This is a PM format, we need to add 12 hours to the hour portion
        if int(cHour) == 12:
            nHour = int(cHour)
        else:
            nHour = int(cHour) + 12
        
        cTimeToReturn = str(nHour) + cRemainingTime
    
    return cTimeToReturn
    

if __name__ == '__main__':
    #fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input("Enter time : ")

    result = timeConversion(s)

    print(result)
    #fptr.write(result + '\n')

    #fptr.close()
