
def convertString(s) -> int:

    # Move from both sides at the same time
    str_s = list(s)
    leftPointer = 0
    rightPointer = len(s) - 1
    swapCounter = 0
    lcharFound = False

    while leftPointer <= rightPointer:
        if str_s[leftPointer] != str_s[rightPointer]:
            # these two characters are not the same. Find a character which is the same and swap
            tempRightPointer = rightPointer
            lcharFound = False
            while str_s[leftPointer] != str_s[rightPointer] and tempRightPointer > leftPointer:
                tempRightPointer -= 1
                if str_s[leftPointer] == str_s[tempRightPointer]:
                    # found our character to swap. Now swap them
                    str_s[rightPointer], str_s[tempRightPointer] = str_s[tempRightPointer], str_s[rightPointer]
                    swapCounter += 1
                    lcharFound = True
            if lcharFound == False:
                swapCounter = -1
                break

        leftPointer+=1
        rightPointer-=1
    
    reverseString = str_s[::-1]
    if str_s != reverseString:
        swapCounter = -1
        
    return swapCounter


print(convertString("0100100"))
print(convertString("101000"))
print(convertString("101100"))