#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'isBalanced' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
#

def isBalanced(s):
    # Write your code here
    # Three opening brackets are (, [ and { and corresponding closing are }])
    # we need to see that if we opened a bracket, unless there is another open
    # bracket, the closing bracket should be of the last starting bracket
    
    # we will make a list of what to expect as a closing bracket. Last in First out
    
    aExpectedClosingBrackets = []
    aOpeningBrackets = ["(", "[", "{"]
    aClosingBrackets = [")", "]", "}"]
    
    for bracket in s:
        if bracket in aOpeningBrackets:
            # this is an opening bracket. Add corresponding ending bracket to the list
            aExpectedClosingBrackets.append(aClosingBrackets[aOpeningBrackets.index(bracket)])
        elif bracket in aClosingBrackets:
            # we found a closing bracket rather than opening bracket
            # see if the list we have in the sequence has this closing bracket or not
            if len(aExpectedClosingBrackets) > 0:
                cClosingBracketExpected = aExpectedClosingBrackets[len(aExpectedClosingBrackets) - 1]
                if cClosingBracketExpected == bracket:
                    aExpectedClosingBrackets.pop(len(aExpectedClosingBrackets) - 1)
                else:
                    return "NO"
            else:
                return "NO"
    
    if len(aExpectedClosingBrackets) > 0:
        return "NO"
    else:
        return "YES"
            
            
if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # t = int(input().strip())

    # for t_itr in range(t):
    #     s = input()

    #     result = isBalanced(s)

    #     fptr.write(result + '\n')

    # fptr.close()
    #print(isBalanced("}][}}(}][))]"))
    #print(isBalanced("()[{}()]([[][]()[[]]]{()})([]()){[]{}}{{}}{}(){([[{}([]{})]])}"))

    s = ["()[{}()]([[][]()[[]]]{()})([]()){[]{}}{{}}{}(){([[{}([]{})]])}",
"{][({(}]][[[{}]][[[())}[)(]([[[)][[))[}[]][()}))](]){}}})}[{]{}{((}]}{{)[{[){{)[]]}))]()]})))[",
"[)](][[([]))[)",
"]}]){[{{){",
"{[(}{)]]){(}}(][{{)]{[(((}{}{)}[({[}[}((}{()}[]})]}]]))((]][[{{}[(}})[){()}}{(}{{({{}[[]})]{((]{[){[",
"()}}[(}])][{]{()([}[}{}[{[]{]](]][[))(()[}(}{[{}[[]([{](]{}{[){()[{[{}}{[{()(()({}([[}[}[{(]})",
"){[])[](){[)}[)]}]]){](]()]({{)(]])(]{(}(}{)}])){[{}((){[({(()[[}](]})]}({)}{)]{{{",
"[(})])}{}}]{({[]]]))]})]",
"[{",
"{}([{()[]{{}}}])({})",
"{({}{[({({})([[]])}({}))({})]})}",
"()[]",
"{)[])}]){){]}[(({[)[{{[((]{()[]}][([(]}{](])()(}{(]}{})[)))[](){({)][}()((",
"[][(([{}])){}]{}[()]{([[{[()]({}[])()()}[{}][]]])}",
"(}]}",
"(([{()}]))[({[{}{}[]]{}})]{((){}{()}){{}}}{}{{[{[][]([])}[()({}())()({[]}{{[[]]([])}})()]]}}",
"[(([){[](}){){]]}{}([](([[)}[)})[(()[]){})}}]][({[}])}{(({}}{{{{])({]]}[[{{(}}][{)([)]}}",
"()()[()([{[()][]{}(){()({[]}[(((){(())}))]()){}}}])]",
"({)}]}[}]{({))}{)]()(](])})][(]{}{({{}[]{][)){}{}))]()}((][{]{]{][{}[)}}{)()][{[{{[[",
")}(()[])(}]{{{}[)([})]()}()]}(][}{){}}})}({](){([()({{(){{",
"}([]]][[){}}[[)}[(}(}]{(}[{}][{}](}]}))]{][[}(({(]}[]{[{){{(}}[){[][{[]{[}}[)]}}]{}}(}"]

for a in s:
    print(isBalanced(a))
