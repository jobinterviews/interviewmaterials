# HackerRank code - It passed
#!/bin/python3
#

import math
import os
import random
import re
import sys

#
# Complete the 'plusMinus' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#

def plusMinus(arr):
    # Write your code here
    # total elements are 
    nTotalElements = len(arr)
    nPositiveNums = 0
    nNegativeNums = 0
    nZeroNums = 0

    # Count for how many are positive and how many are negative
    for nVal in arr:
        if nVal > 0:
            nPositiveNums += 1
        elif nVal < 0:
            nNegativeNums += 1
        else:
            nZeroNums += 1
        
    print("{:0.4f}".format(nPositiveNums / nTotalElements))
    print("{:0.4f}".format(nNegativeNums / nTotalElements))
    print("{:0.4f}".format(nZeroNums / nTotalElements))
        
    
if __name__ == '__main__':
    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    plusMinus(arr)
