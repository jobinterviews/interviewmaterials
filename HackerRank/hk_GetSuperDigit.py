#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'superDigit' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. STRING n
#  2. INTEGER k
#

def superDigit(n, k):
    # Write your code here
    # First create a digit p which is n string k times
    #p=n*k
    p=n
    # now go through each digit in the string and start adding them up
    while len(p) > 1:
        cNewNumber = 0
        for eachDigit in p:
            cNewNumber += int(eachDigit)
        # now we have a sum of all digits. make it a string
        p = str(cNewNumber)
    
    # now go through k times
    p = p * k
    while len(p) > 1:
        cNewNumber = 0
        for eachDigit in p:
            cNewNumber += int(eachDigit)
        # now we have a sum of all digits. make it a string
        p = str(cNewNumber)
                
    return int(p)
    
    
    
    
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = first_multiple_input[0]

    k = int(first_multiple_input[1])

    result = superDigit(n, k)

    fptr.write(str(result) + '\n')

    fptr.close()
