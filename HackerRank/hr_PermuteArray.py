A = [4,4,3,2,1,4,4,3,2,4]
B = [2,3,0,1,1,3,1,0,0,2]
k = 4


isYesOrNo = "YES"
alreadyChecked = []
newArray = [0 for i in range(len(A))]
A.sort()
# we need to move the array in such a way that sum is k
# lets start with array a and add each value with K and see if we have
# a value greater than or equal to that value
# keep on deleting these values from b so we do not use them again
for A_i, A_val in enumerate(A):
    searchInB = k - A_val
    didNotFind = True
    for B_i, B_val in enumerate(B):
        if B_i not in alreadyChecked:
            if B_val >= searchInB:
                newArray[A_i] = B_val
                alreadyChecked.append(B_i)
                didNotFind = False
                break
            
    if didNotFind:
        isYesOrNo = "NO"
        break
        
# if 0 in newArray:
#     isYesOrNo = "NO"

# return isYesOrNo
print(newArray)