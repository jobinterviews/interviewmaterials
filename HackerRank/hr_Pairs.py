#!/bin/python3

import math
import os
import random
import re
import sys


#
# Complete the 'pairs' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER k
#  2. INTEGER_ARRAY arr
#

def pairs(k, arr):
    # Write your code here
    
    # convert array to set() it will reduce to no duplicates and faster to search
    arr2Search = set(arr)
    
    # also sort the array 
    arr.sort()
    maxNumber = arr[-1] # last number is highest now
    
    # go through each number and subtract it with k. then see if there is a number in 
    # array which is the remaining
    nTotalPairs = 0
    for num in arr:
        remaining = num + k
        if remaining in arr2Search:
            nTotalPairs += 1
            
        if remaining >= maxNumber:
            break
            
    return nTotalPairs
if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # first_multiple_input = input().rstrip().split()

    # n = int(first_multiple_input[0])

    # k = int(first_multiple_input[1])

    # arr = list(map(int, input().rstrip().split()))

    # result = pairs(k, arr)

    # fptr.write(str(result) + '\n')

    # fptr.close()

    print(pairs(1, [1,2,3,4]))
