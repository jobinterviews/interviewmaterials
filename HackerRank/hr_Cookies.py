# Write your code here
from logging import exception


A=[6214,8543,9266,1150,7498,7209,9398,1529,1032,7384,6784,34,1449,7598,8795,756,7803,4112,298,4967,1261,1724,4272,1100,9373]
k = 3581
# First of all sort the array in ascending order
A.sort()

# Formula is get two minimum values out of the array
firstMin = A[0]
A.pop(0)
# now pop it out of the array
secondMin = A[0]
A.pop(0)

totalSteps = 0
while firstMin < k:
    result = firstMin + (2 * secondMin)
    A.append(result)
    totalSteps += 1
    A.sort()
    try:
        # Formula is get two minimum values out of the array
        firstMin = A[0]
        A.pop(0)
        # now pop it out of the array
        secondMin = A[0]
        A.pop(0)        
    except:
        totalSteps = -1
        break

if totalSteps == 0:
    print(-1)
else:
    print(totalSteps)