s = "aaab"
nReturnIndex = -1
cNewString = ""
for index, char in enumerate(s):
    cNewString = [c for i, c in enumerate(s) if i is not index]
    if cNewString == cNewString[::-1]:
        nReturnIndex = index
        break