grid = ['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']

# Determine how manu rows and columns we have
nTotalRows = len(grid)
nTotalCols = len(grid[0])

# Now sort each row in the grid
aSortedGrid = []

for eachRow in grid:
    lstChars = sorted(list(eachRow))
    cNewRow = ""
    for eachChar in lstChars:
        cNewRow += eachChar
    #Now add it back to a new grid
    aSortedGrid.append(cNewRow)
    
# Now see if each row is in ascending order or not
isAscending = True
for colCount in range(nTotalCols):
    cNewColString = ""
    for rowCount in range(nTotalRows):
        cNewColString += aSortedGrid[rowCount][colCount]
    
    # now sort this string
    cSortedColumn = ""
    lstChars = sorted(list(cNewColString))
    for eachChar in lstChars:
        cSortedColumn += eachChar
                    
    if cNewColString != cSortedColumn:
        isAscending = False
        break

if isAscending:
    print("YES")
else:
    print("NO")