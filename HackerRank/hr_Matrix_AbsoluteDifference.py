# Given a square matrix, calculate the absolute difference between the sums of its diagonals.

# For example, the square matrix  is shown below:

# 1 2 3
# 4 5 6
# 9 8 9  
# The left-to-right diagonal = . The right to left diagonal = . Their absolute difference is .

# Function description

# Complete the  function in the editor below.

# diagonalDifference takes the following parameter:

# int arr[n][m]: an array of integers
# Return

# int: the absolute diagonal difference
# Input Format

# The first line contains a single integer, , the number of rows and columns in the square matrix .
# Each of the next  lines describes a row, , and consists of  space-separated integers .

# Constraints

# Output Format

# Return the absolute difference between the sums of the matrix's two diagonals as a single integer.

# Sample Input

# 3
# 11 2 4
# 4 5 6
# 10 8 -12
# Sample Output

# 15
# Explanation

# The primary diagonal is:

# 11
#    5
#      -12
# Sum across the primary diagonal: 11 + 5 - 12 = 4

# The secondary diagonal is:

#      4
#    5
# 10
# Sum across the secondary diagonal: 4 + 5 + 10 = 19
# Difference: |4 - 19| = 15

# Note: |x| is the absolute value of x

# Language
# Python 3

# More
# 1920212223242526272829303132333435363738394041424344454647484950515253
    
#     for rowNum in range(0, nNumOfRows):
#         nFirstDiagonal += arr[rowNum][rowNum]
#         nSecondDiagonal += arr[rowNum][(nNumOfColumns - 1) - rowNum]

#     return (abs(nFirstDiagonal - nSecondDiagonal))

# if __name__ == '__main__':
#     fptr = open(os.environ['OUTPUT_PATH'], 'w')







# Line: 36 Col: 16

# Submit Code

# Run Code

# Upload Code as File

# Test against custom input
# Congratulations
# You solved this challenge. Would you like to challenge your friends?Share on FacebookShare on TwitterShare on LinkedIn
# Next Challenge

# Test case 0

# Test case 1

# Test case 2

# Test case 3

# Test case 4

# Test case 5

# Test case 6

# Test case 7

# Test case 8

# Test case 9

# Test case 10
# Compiler Message
# Success
# Input (stdin)

# Download
# 3
# 11 2 4
# 4 5 6
# 10 8 -12
# Expected Output

# Download
# 15


#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'diagonalDifference' function below.
#
# The function is expected to return an INTEGER.
# The function accepts 2D_INTEGER_ARRAY arr as parameter.
#

def diagonalDifference(arr):
    # Write your code here
    # Matrix looks like
    # X X X
    # Y Y Y 
    # Z Z Z
    
    # for first diagnoal we start with arr[0] and pick up [0] column
    # for next row it will be arr[1] and [1] column
    # next it will be arr[2][2] and so it for all rows 
    # and sum it
    nFirstDiagonal = 0
    nSecondDiagonal = 0
    nNumOfRows = len(arr)
    nNumOfColumns = len(arr[0]) # assuming all rows have the same number of columns
    
    for rowNum in range(0, nNumOfRows):
        nFirstDiagonal += arr[rowNum][rowNum]
        nSecondDiagonal += arr[rowNum][(nNumOfColumns - 1) - rowNum]

    return (abs(nFirstDiagonal - nSecondDiagonal))

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    arr = []

    for _ in range(n):
        arr.append(list(map(int, input().rstrip().split())))

    result = diagonalDifference(arr)

    fptr.write(str(result) + '\n')

    fptr.close()
