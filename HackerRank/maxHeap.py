# ======================================  This is work in progress  ====================================
# Implement maxHeap in python. Not using heapq library to understand what we are doing
# first node is node 1 and not 0
# parent = node // 2
# child: Left = node * 2
#        right = (node * 3) + 1
# Push() add an item to the list
# Peek() get the max item from the list
# floor() get the min item from the list
# PopMax() remove max item from the list
# PopMin() remove the min item from the list

from ast import Break
from turtle import clear

class maxHeap:
    # initialize the heap with a list of items if provided
    def __init__(self, items = []):
        super().__init__()
        self.root = 1
        self.heap = [0]
        self.floor = []
        self.total = 0
        # items will be values we can initialize this heap while creating it
        for item in items:
            self.heap.append(item)  # Add the item to the heap
            self.AdjustValue(len(self.heap) - 1)

        self.total = len(self.heap)
        if self.total > 0:
            self.__heapFloor()

    # Determine each [left and right] child of a parent
    # Get: Parent as parent index 
    def LeftChild(self, Parent):
        return Parent * 2

    def RightChild(self, Parent):
        return (Parent * 2) + 1

    # Determine floor of the heap
    def __heapFloor(self):
        self.floor = []
        if self.heap[1]:
            parentIndex = 1
            # now get the children to this parent
            leftChildIndex  = parentIndex * 2
            rightChildIndex = leftChildIndex + 1

            processingIndex = leftChildIndex
            indexOfParentNow = 0
            while processingIndex < self.total:
                # first find bottom of the left side
                indexOfParentNow = processingIndex
                processingIndex = indexOfParentNow * 2

            # We have the value of the left now
            self.floor.append(indexOfParentNow)

            processingIndex = rightChildIndex
            indexOfParentNow = 0
            while processingIndex < self.total:
                # first find bottom of the left side
                indexOfParentNow = processingIndex
                processingIndex = (indexOfParentNow * 2) + 1

            # We have the value of the left now
            self.floor.append(indexOfParentNow)
        else:
            return
        
    # move the item up in the list till it reaches it correct position
    def AdjustValue(self, index):
        # first get the parent of the index we are working with 
        parentIndex = index // 2
        # if this value is 1 or less than 1 than leave it
        if parentIndex <= 1:
            return
        elif self.heap[parentIndex] < self.heap[index]:
            # parent is less than the index supplied so swap them
            self.__swap(index, parentIndex)
            self.AdjustValue(parentIndex)

    # we need a function to swap two items in a heap and it is an internal function
    def __swap(self, index1, index2):
        self.heap[index1], self.heap[index2] = self.heap[index2], self.heap[index1]

    # Push a value to the heap
    def Push(self, data):
        self.heap.append(data)
        self.AdjustValue(len(self.heap) - 1)

    # Peek will return the top most value of the heap which is max
    def Peek(self):
        if self.heap[1]:
            return self.heap[1]
        else:
            return False

if __name__=='__main__':
    # define an object of class maxHeap
    itemsToAdd = [24,16,1,5,11,12,2,3,5,19]
    myHeap = maxHeap(itemsToAdd)
    print(myHeap.heap)
