s = "!m-rB`-oN!.W`cLAcVbN/CqSoolII!SImji.!w/`Xu`uZa1TWPRq`uRBtok`xPT`lL-zPTc.BSRIhu..-!.!tcl!-U"
k = 62
# expected: 
# !w-bL`-yX!.G`mVKmFlX/MaCyyvSS!CSwts.!g/`He`eJk1DGZBa`eBLdyu`hZD`vV-jZDm.LCBSre..-!.!dmv!-E
# !w-bL`-yX!.G`mVKmFlX/MaCyyvSS!CSwts.!g/`He`eZk1DGZBa`eBLdyu`hZD`vV-jZDm.LCBSre..-!.!dmv!-E
# !m-rB`-oN!.W`cLAcVbN/CqSoolII!SImji.!w/`Xu`uZa1TWPRq`uRBtok`xPT`lL-zPTc.BSRIhu..-!.!tcl!-U


# Write your code here
# We will only be replacing 65=A to 90=Z and similarly 97=a to 122=z
cNewString = ""
nCharCounter = 0
nNewASCII = 0
cCharToAdd = ""

for char in s:
    cCharToAdd = char
    if ord(char) in range(65,91):
        # This is our character. Change the number 
        nNewASCII = ord(char) + k
        # Now this new character might need to rotate
        while nNewASCII > 90:
            nIncreasedBy = nNewASCII - 90 - 1
            nNewASCII = 65 + nIncreasedBy
        # Now update the character
        cCharToAdd = chr(nNewASCII)

    if ord(char) in range(97,123):
        # This is our character. Change the number 
        nNewASCII = ord(char) + k
        # Now this new character might need to rotate
        while nNewASCII > 122:
            nIncreasedBy = nNewASCII - 122 - 1
            nNewASCII = 97 + nIncreasedBy
        # Now update the character
        cCharToAdd = chr(nNewASCII)

    cNewString = cNewString+''.join(cCharToAdd)
    nCharCounter += 1

print(cNewString)