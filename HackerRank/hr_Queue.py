# '''
# A Queue is FIFO = First in, First Out list of entries which has a link to the next node
# We need to define a node first.
# NODE: It is a combination of Data and next. 
#     - Data: is Data for this node
#     - Next: is the address of the next node in the queue

# QUEUE: It is a list of nodes linked together with each other
#     - Add or enQueue : Goto the end of the queue and add it to end by updating last node next to this node
#     - Remove or deQueue: GoTo the start of the queue and then move to the next node, make root node association to this second node and return the dropped node
# '''
class Node:
    def __init__(self, data, next = None):
        self.data = data
        self.next = next

class Queue:
    def __init__(self):
        self.rootNode = None
        self.lastNode = None

    def enQueue(self, addNode : Node):
        # We add it to the end of the queue
        if self.rootNode is None:
            # There is no node in the data
            self.rootNode = addNode
            self.lastNode = addNode
        else:
            # There are nodes in the queue. Make last node point to this new node
            self.lastNode.next = addNode    # First assign to this incoming node
            self.lastNode = addNode         # now move the pointer to this node

    def deQueue(self) -> Node:
        # we do not need a node but we will go to the start of the Queue and move the rootNode association
        # to the second node but before we do so, we will capture the first node so we can return that
        nodeToRemove = self.rootNode
        self.rootNode = nodeToRemove.next
        return nodeToRemove

    def show(self):
        # Show this queue. We will start from the root node and will go to all the nodes will we hit None in the next
        iNode = self.rootNode
        while iNode.next is not None:
            print(iNode.data, " - ")
            iNode = iNode.next

if __name__ == "__main__":
    q = Queue() # Instanciate the class
    commands = ["10 ", "1 42", "2 ", "1 14", "3 ", "1 28", "3 ", "1 60", "1 78", "2 ", "2 "]

    # now the first entry is how many commands will be executed
    for command in commands:
        executeWhat, executeData = command.split(" ")
        if executeWhat == "1":      # Add a new node to the queue
            q.enQueue(Node(executeData))
        elif executeWhat == "2":    # Remove a node from the queue
            deletedNode = q.deQueue()
            print(deletedNode.data)
        elif executeWhat == "3":    # Show a list of all nodes on the screen
            q.show()
        else:
            print("Invalid option")