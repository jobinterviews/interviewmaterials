#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'miniMaxSum' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#

def miniMaxSum(arr):
    # Write your code here
    arr.sort()
    nMinSum = 0 
    nMaxSum = 0 
    for i in arr[:4]:
        nMinSum+=i

    for i in range(1, len(arr)):
        nMaxSum+=arr[i]

    print(f"{nMinSum} {nMaxSum}")
    
if __name__ == '__main__':

    arr = list(map(int, input().rstrip().split()))

    miniMaxSum(arr)