#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'birthday' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER_ARRAY s
#  2. INTEGER d
#  3. INTEGER m
#

def birthday(s, d, m):
    # Write your code here
    noOfWaysBarIsDivided = 0
    #s=bar with numbers
    # d = birth day
    # m = birth month
    
    # allDigits = set(s)
    
    # segment is what numbers to use that can sum up to be day
    # no of items in the segment should be equal to month
    
    # go through each digit and compare it against all other numbers 
    # make pairs to exact number of month and see if we have an exact
    # match to the day
    counter = 0
    
    # s=[1,2,1,3,2]
    # d=3
    # m=2
    
    alreadyProcessedIndex = []
    pairUsed = []
    
    # for n in allDigits:
    for i,n in enumerate(s):
        if i not in alreadyProcessedIndex:
            remainingSum = d - n
            counter = 1
            # alreadyProcessedIndex.append(i)
            pairUsed = []
            pairUsed.append(i)
            # print(f"Each loop i={i} remaining={remainingSum}")                
            # for n2 in allDigits:
            for i2, n2 in enumerate(s):
                if i2 not in alreadyProcessedIndex and i2 != i:
                    # print(f"i2 which passes is {i2}")
                    if counter < m and n2 <= remainingSum:
                        remainingSum -= n2
                        counter += 1
                        pairUsed.append(i2)
                        # print(f"Each loop i2={i2} remaining={remainingSum}")
                    
                    if remainingSum == 0:
                        break
            if remainingSum == 0 and counter == m:
                noOfWaysBarIsDivided+=1
                alreadyProcessedIndex += pairUsed
    
    return noOfWaysBarIsDivided

if __name__ == '__main__':
    s=[2,5,1,3,4,4,3,5,1,1,2,1,4,1,3,3,4,2,1]
    d=18
    m=7
    print(birthday(s, d, m))
#     fptr = open(os.environ['OUTPUT_PATH'], 'w')

#     n = int(input().strip())

#     s = list(map(int, input().rstrip().split()))

#     first_multiple_input = input().rstrip().split()

#     d = int(first_multiple_input[0])

#     m = int(first_multiple_input[1])

#     result = birthday(s, d, m)

#     fptr.write(str(result) + '\n')

#     fptr.close()
