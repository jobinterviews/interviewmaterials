# Make a 32 bit map in a list

bitMap = [1]

for i in range(1,32):
    bitMap.append(bitMap[-1]*2)

print(bitMap)

# Now map this number coming in into the bitmap
remainingToPlace = 0

bitConversion = [0 for i in range(32)]

while remainingToPlace > 0:
    for i, b in enumerate(reversed(bitMap)):
        if b <= remainingToPlace:
            # we found a position which needs to be filled
            bitConversion[i] = 1
            remainingToPlace -= b

print(bitConversion)

# now convert it into reversed
for i in range(32):
    if bitConversion[i] == 0:
        bitConversion[i] = 1
    else:
        bitConversion[i] = 0

print(bitConversion)

retValue = 0
for i, nNum in enumerate(reversed(bitConversion)):
    if nNum == 1:
        retValue += bitMap[i]

print(retValue)
