import queue as Queue

cntr = 0

class Node:
    def __init__(self, freq, data):
        self.freq = freq
        self.data = data
        self.left = None
        self.right = None
        global cntr
        self._count = cntr
        cntr = cntr + 1
        
    def __lt__(self, other):
        if self.freq != other.freq:
            return self.freq < other.freq
        return self._count < other._count

def huffman_hidden():#builds the tree and returns root
    q = Queue.PriorityQueue()

    
    for key in freq:
        q.put((freq[key], key, Node(freq[key], key) ))
    
    while q.qsize() != 1:
        a = q.get()
        b = q.get()
        obj = Node(a[0] + b[0], '\0' )
        obj.left = a[2]
        obj.right = b[2]
        q.put((obj.freq, obj.data, obj ))
        
    root = q.get()
    root = root[2]#contains root object
    return root

def dfs_hidden(obj, already):
    if(obj == None):
        return
    elif(obj.data != '\0'):
        code_hidden[obj.data] = already
        
    dfs_hidden(obj.right, already + "1")
    dfs_hidden(obj.left, already + "0")

"""class Node:
    def __init__(self, freq,data):
        self.freq= freq
        self.data=data
        self.left = None
        self.right = None
"""        

# Enter your code here. Read input from STDIN. Print output to STDOUT
def decodeHuff(root, s):
	#Enter Your Code Here

    # s is the sequence of moves we will perform on the tree
    # 0 is left and 1 is right
    # we will keep on moving from the root till we find data
    # then we will move back to the root
    
    strOffset = 0
    cDecodedString = ""
    currentNode = root
    lenOfCommands = len(s)
    
    while strOffset < lenOfCommands:
        thisMove = s[strOffset]
        
        if thisMove == '1':
            # move to the right and see if we have data there
            if not currentNode.right:
                currentNode = root
            else:
                rightNode = currentNode.right
                currentNode = rightNode
                if currentNode.data != '\0':
                    cDecodedString += currentNode.data
                    currentNode = root
                    
        if thisMove == '0':
            # move to the right and see if we have data there
            if not currentNode.left:
                currentNode = root
            else:
                leftNode = currentNode.left
                currentNode = leftNode
                if currentNode.data != '\0':
                    cDecodedString += currentNode.data
                    currentNode = root
        
        # print(f"{strOffset} each loop {currentNode.data}={currentNode.left.data}-{currentNode.right.data}" )
        strOffset += 1
        
    print(cDecodedString)

    
# Sample code: Rumpelstiltskin
root = huffman_hidden()#contains root of huffman tree

code_hidden = {}#contains code for each object

dfs_hidden(root, "")

if len(code_hidden) == 1:#if there is only one character in the i/p
    for key in code_hidden:
        code_hidden[key] = "0"

toBeDecoded = ""

for ch in ip:
   toBeDecoded += code_hidden[ch]

decodeHuff(root, toBeDecoded)