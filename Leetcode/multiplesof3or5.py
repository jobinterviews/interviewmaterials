# Python If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.

nLessThan = int(input("Enter less than number range will be [0 to number entered]"))
nTotal = 0
nLessThan = 1000

for counter in range(nLessThan):
    if counter % 3 == 0 or counter % 5 == 0:
        nTotal += counter 

print(nTotal)