logs = ["88 99 200", "88 99 300","99 32 100","12 12 15"]
threshold = 2
# Write your code here
# logs has a list of records. It is a list of strings
# First calculate number of transactions per IDs
logStatus = {}
aReturnList = []

for cLog in logs:
    # each record is a string of IDs separated with space
    thisLog = cLog.split(' ')
    # make a set to make sure we have only unique numbers
    cUniqueList = set(thisLog)
    for eachLogID in cUniqueList:
        # now we are at a log ID level. See if we have that ID or we need to add it
        nValue = logStatus.get(eachLogID)
        if nValue is None:
            logStatus.update({eachLogID:1})
        else:
            logStatus.update({eachLogID:nValue+1})

# now that we have a list of a status of IDs. See if any has been above their threshold
for key, value in logStatus.items():
    if value >= threshold:
        # This id is greater than the threshold
        aReturnList.append(key)
        
aReturnList.sort()
print(aReturnList)