# A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
# Find the largest palindrome made from the product of two 3-digit numbers.

# three digit numbers will be 100 to 999. We read left to right, it is 9009 we read right to left it is still 9009

def flipNumber(cNumber):
    counter = len(cNumber) - 1
    cNewNumber = ""
    for sub in range(len(cNumber)):
        cNewNumber += cNumber[counter]
        counter -= 1

    return int(cNewNumber)
result = 0
maxNumber = 0
firstNum = 0
secondNum = 0

for firstNumber in range(100, 1000):
    for secondNumber in range(100, 1000):
        result = firstNumber * secondNumber
        #flipResult = flipNumber(str(result))
        flipResult = int(str(result)[::-1])     # This will start = beginning, end is all characters but start from the end -1
        if result == flipResult:
            if result > maxNumber:
                maxNumber = result
                firstNum = firstNumber
                secondNum = secondNumber

print(f"{firstNum} by {secondNum} = {maxNumber}")
