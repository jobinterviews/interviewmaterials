n=15
# Write your code here
for i in range(1,n+1):
    if i%3 == 0 and i%5 == 0:
        # it is a multiple of 3 and 5 
        print("FizBizz")
    elif i%3 == 0:
        print("Fizz")
    elif i%5 == 0:
        print("Buzz")
    else:
        print(i)