# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
# What is the 10 001st prime number?

nThPrimeNumber = int(input("What prime number to find : "))
counter = 0
number = 2

while counter < nThPrimeNumber:
    isPrime = True
    for i in range(2, number):
        if number % i == 0:
            isPrime = False
            break

    if isPrime:
        counter+=1
        primeNumber = number

    number+=1

print(f"{nThPrimeNumber} th Prime Number is {primeNumber}")