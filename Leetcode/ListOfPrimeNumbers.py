# Create a list of all prime numbers to a max limit
maxNumber = int(input("Max number to create a list of all prime numbers [2 to XXX]: "))

# def checkIfPrime(number):
#     for primeNum in range(2, number) :
#         if number%primeNum == 0:
#             return False
#     return (True)

#allPrimeNumbers = [number for number in range(2, maxNumber) if checkIfPrime(number)]
allPrimeNumbers = [number for number in range(2, maxNumber) if all(number % y != 0 for y in range(2, number))]
print(allPrimeNumbers)