# The prime 41, can be written as the sum of six consecutive primes:
# 41 = 2 + 3 + 5 + 7 + 11 + 13
# This is the longest sum of consecutive primes that adds to a prime below one-hundred.
# The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.
# Which prime, below one-million, can be written as the sum of the most consecutive primes?

def checkIfIsAPrime(number):
    isPrime = True
    for i in range(2, number):
        if number%i == 0:
            isPrime = False
            break
    return isPrime

maxNumber = int(input("Consecutive prime sum less than ?: "))
answer = 0
numberToCheck = 2   # it is the first prime number. 0 and 1 will not be considered
sumIsAPrime = 0
primeNumbers = []
sumPrimeNums = []
sumReverse = 0
reversePrime = 0
# primeNumbers = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73]
# print(sum(primeNumbers))

while answer < maxNumber:
    if answer == 874:
        pass

    if checkIfIsAPrime(numberToCheck):
        primeNumbers.append(numberToCheck)
        answer += numberToCheck
        sumPrimeNums.append(answer)
        print(f"Prime Numbers added {numberToCheck} - {answer}")
        if checkIfIsAPrime(answer):
            sumIsAPrime = answer
        else:
            # this is not a prime number. See if starting from the end can hit a prime number
            # which is higher than sum we are working with
            sumReverse = 0
            for i in reversed(primeNumbers):
                # These are all prime numbers. Just add and see if there is a prime AFTER it is greater than sumInAPrime
                sumReverse+= i
                if checkIfIsAPrime(sumReverse) and sumReverse < maxNumber:
                    reversePrime = sumReverse
                    # if sumReverse > sumIsAPrime and sumReverse < maxNumber:
                    #     sumIsAPrime = sumReverse
                # else:
                #     break

    numberToCheck+=1

print(sumIsAPrime, reversePrime)
