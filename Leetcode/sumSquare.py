# The sum of the squares of the first ten natural numbers is, 1sq + 2sq + 3sq .. 10sq = 385
# The square of the sum of the first ten natural numbers is, 1 + 2 + 3 ... + 10)square = 3025
# Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is .
# 3025 - 385 = 2640
# Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

# Had an error here: it was range should be 101 because 100 is included in the calculations.
nSumSquares = sum([i**2 for i in range(1, 101)])
nSumThenSquare = sum([i for i in range(1, 101)])**2
print(nSumThenSquare - nSumSquares)