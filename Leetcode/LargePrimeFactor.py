# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?
# Prime numbers are 1, 2, 3, 5, 7

def createPrimeNumbers(number) -> list:
    primes = []
    for num in range(2, number):
        # now find the next prime number
        isPrimeNumber = True;
        for checkNum in range(2, num):  # range will not include num here
            if num % checkNum == 0:
                isPrimeNumber = False
        
        if isPrimeNumber:
            primes.append(num)
    
    return primes

def findSmallestPrimeNum(number, allPrimeNumbers):
    # Which is the smallest prime number that we can use here. It should divide this number without a remainder
    for checkPrimeNum in allPrimeNumbers:
        if number % checkPrimeNum == 0:
            return checkPrimeNum
    return 0

nNumber = int(input("Enter a number to calculate largest primefactor of : "))
remainder = nNumber
allPrimeNumbers = createPrimeNumbers(10000) # Todo: We need to determine as we go along not start with 10000
maxPrimeFactor = 0
nPrimeFactors = []

while remainder > 1:
    primeNumber = findSmallestPrimeNum(remainder, allPrimeNumbers)
    if primeNumber > 0:
        nPrimeFactors.append(primeNumber)
        remainder = remainder / primeNumber
        if primeNumber > maxPrimeFactor:
            maxPrimeFactor = primeNumber
 

print(maxPrimeFactor)