# Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, the first 10 terms will be:
# 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
# By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.

nSumOfEvenNumbers = 2        # Start with 3 as we already have 1 and 2 to start with
nPreviousNum = 1
nNewNum = 2
nFibonacciNum = 2        # start with 3 as the next FibonacciNumber. We will create 3 from these two numbers

while nFibonacciNum < 4000000:
    nFibonacciNum = nPreviousNum + nNewNum
    if nFibonacciNum % 2 == 0:
        nSumOfEvenNumbers+=nFibonacciNum
    nPreviousNum = nNewNum
    nNewNum = nFibonacciNum

print(nSumOfEvenNumbers)